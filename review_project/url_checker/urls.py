from django.urls import path
from url_checker.views import UrlGetAPIView

urlpatterns = [
    path('',UrlGetAPIView.as_view(),name='url-check')
]