import time
from django.http import JsonResponse
from rest_framework.views import APIView
import requests
import json
from django.core.cache import cache


# Create your views here.


class UrlGetAPIView(APIView):

    def get(self,request,format=None):

        def hit_url():
            if cache.get('final_data'):
                print('here')
                response_data = cache.get('final_data')
            else:
                response_data = requests.get('https://reqres.in/api/unknown')
            return response_data


        for counter in range(1,6):
            response_data = hit_url()
            if cache.get('final_data') or response_data.status_code == 200 :
                if cache.get('final_data'):
                    return JsonResponse ({'data':response_data})
                else:
                    cache.set('final_data',json.loads(response_data.text),5)

                return JsonResponse ({'data':json.loads(response_data.text)})
            else:
                time.sleep(counter)
                print(f'retrying for {counter} time')

        return JsonResponse ({'info':'data resurrection filed'})

        
        
